var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var browserSync = require('browser-sync');
var csso = require('gulp-csso');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var webpack = require('webpack');
var webpackStream = require('webpack-stream-fixed');
var zip = require('gulp-zip');
var files = ['./ht-movie.php', './extensions/**'];
var wpPot       = require( 'gulp-wp-pot' );

function swallowError(error) {
	//Prints details of the error in the console
	console.log(error.toString());
	this.emit('end');
}

gulp.task('webpack', function() {
	return gulp
		.src('./src/js/app/app.js')
		.pipe(webpackStream(require('./webpack.config.js'), webpack))
		.on('error', swallowError)
		.pipe(gulp.dest('./extensions/ht-movie/static/js'));
});

gulp.task('minify', function() {
	//Setting ENV to production so Webpack will minify JS files.
	process.env.NODE_ENV = 'production';
	return gulp
		.src('./src/js/app/app.js')
		.pipe(webpackStream(require('./webpack.config.js'), webpack))
		.pipe(gulp.dest('./extensions/ht-movie/static/js'));
});

gulp.task('zip', ['minify'], function() {
	gulp
		.src(files, { base: './' })
		.pipe(zip('ht-movie.zip'))
		.pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
	//Webpack will watch the asser files. All we need is to watch the compiled files.
	gulp.watch('./extensions/ht-movie/static/js/*.js').on('change', browserSync.reload);
});

gulp.task('sync', function() {
	var options = {
		proxy: 'http://localhost/buster/',
		port: 3000,
		files: ['**/*.php'],
		ghostMode: {
			clicks: false,
			location: false,
			forms: false,
			scroll: false,
		},
		injectChanges: true,
		logFileChanges: true,
		logLevel: 'debug',
		logPrefix: 'gulp-patterns',
		notify: true,
		reloadDelay: 0,
	};
	browserSync(options);
});

/*CREATE .POT FILE*/
gulp.task( 'pot', () => {
    gulp.src( 'extensions/**/*.php' )
        .pipe( wpPot( {
            domain: 'blockter',
            package: 'Haintheme'
        } ) )
        .on( 'error', swallowError )
        .pipe( gulp.dest( 'languages/' + 'ht-movie' + '.pot' ) );
} );

gulp.task('default', ['webpack', 'watch', 'sync', 'pot']);
