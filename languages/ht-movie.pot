# Copyright (C) 2020 Haintheme
# This file is distributed under the same license as the Haintheme package.
msgid ""
msgstr ""
"Project-Id-Version: Haintheme\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: extensions/ht-movie/class-fw-extension-ht-movie.php:216, extensions/ht-movie/settings-options.php:234
msgid "Movie"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:217, extensions/ht-movie/manifest.php:7
msgid "HT Movie"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:239
msgid "Create a movie"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:324, extensions/ht-movie/settings-options.php:235
msgid "TV Show"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:325
msgid "HT TV Show"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:346
msgid "Create a show"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:386, extensions/ht-movie/settings-options.php:291, extensions/ht-movie/settings-options.php:505
msgid "Genre"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:387
msgid "Genres"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:431, extensions/ht-movie/settings-options.php:304, extensions/ht-movie/settings-options.php:518
msgid "Collection"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:432
msgid "Collections"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:476, extensions/ht-movie/settings-options.php:236, extensions/ht-movie/settings-options.php:265, extensions/ht-movie/settings-options.php:492, extensions/ht-movie/views/single-movie.php:197, extensions/ht-movie/views/single-movie.php:351, extensions/ht-movie/views/single-movie.php:603, extensions/ht-movie/views/single-show.php:186, extensions/ht-movie/views/single-show.php:331, extensions/ht-movie/views/single-show.php:627
msgid "Cast"
msgstr ""

#: extensions/ht-movie/class-fw-extension-ht-movie.php:477
msgid "Casts"
msgstr ""

#: extensions/ht-movie/hooks.php:569, extensions/ht-movie/hooks.php:570
msgid "Settings"
msgstr ""

#: extensions/ht-movie/hooks.php:584, extensions/ht-movie/hooks.php:603, extensions/ht-movie/views/single-movie.php:94, extensions/ht-movie/views/single-show.php:86
msgid "Watch Trailer"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:9, extensions/ht-movie/options/posts/ht_show.php:9
msgid "Movie Detail"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:13, extensions/ht-movie/options/posts/ht_show.php:13
msgid "General"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:18, extensions/ht-movie/settings-options.php:356
msgid "Tagline"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:22, extensions/ht-movie/options/posts/ht_show.php:18, extensions/ht-movie/settings-options.php:369, extensions/ht-movie/settings-options.php:570, extensions/ht-movie/views/single-movie.php:195, extensions/ht-movie/views/single-show.php:183
msgid "Overview"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:26, extensions/ht-movie/settings-options.php:382
msgid "Director"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:33, extensions/ht-movie/settings-options.php:395, extensions/ht-movie/settings-options.php:596
msgid "Writer"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:39, extensions/ht-movie/settings-options.php:408
msgid "Release Date"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:43, extensions/ht-movie/settings-options.php:421
msgid "Runtime"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:47, extensions/ht-movie/options/posts/ht_show.php:36, extensions/ht-movie/settings-options.php:434, extensions/ht-movie/settings-options.php:635
msgid "Production"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:51, extensions/ht-movie/options/posts/ht_show.php:40, extensions/ht-movie/settings-options.php:447, extensions/ht-movie/settings-options.php:648
msgid "Country"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:55, extensions/ht-movie/options/posts/ht_show.php:44
msgid "Languages"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:60, extensions/ht-movie/options/posts/ht_show.php:49, extensions/ht-movie/views/single-movie.php:196, extensions/ht-movie/views/single-movie.php:301, extensions/ht-movie/views/single-show.php:185, extensions/ht-movie/views/single-show.php:278
msgid "Media"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:65, extensions/ht-movie/options/posts/ht_show.php:54, extensions/ht-movie/settings-options.php:330, extensions/ht-movie/settings-options.php:544
msgid "Banner"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:70, extensions/ht-movie/options/posts/ht_show.php:59
msgid "Gallery"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:75
msgid "Video Youtube URLs"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:76, extensions/ht-movie/options/posts/ht_show.php:65
msgid "Enter Youtube video key, link this: ue80QwXMRHg"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:80
msgid "This option will replace Youtube video URLs"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:98, extensions/ht-movie/options/posts/ht_movie.php:134
msgid "Thumbnail"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:99, extensions/ht-movie/options/posts/ht_movie.php:135
msgid "Set the movie thumbnail"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:109, extensions/ht-movie/options/posts/ht_movie.php:145
msgid "Add"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:111
msgid "Hosted Video URLs"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:112
msgid "Enter link of the video here, support: .mp4 & .webm"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:116
msgid "This option will re  place Youtube video URLs & Hosted URL"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:147
msgid "Iframe Video Code"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:148
msgid "Embed Iframe of the video here"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:153, extensions/ht-movie/options/posts/ht_show.php:100
msgid "Button"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:158, extensions/ht-movie/options/posts/ht_show.php:105
msgid "Button 1 Text"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:162, extensions/ht-movie/options/posts/ht_show.php:109
msgid "Button 1 URL"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:166, extensions/ht-movie/options/posts/ht_show.php:113
msgid "Button 2 Text"
msgstr ""

#: extensions/ht-movie/options/posts/ht_movie.php:170, extensions/ht-movie/options/posts/ht_show.php:117
msgid "Button 2 URL"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:22, extensions/ht-movie/settings-options.php:583
msgid "Creator"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:28, extensions/ht-movie/settings-options.php:609
msgid "First Air Date"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:32, extensions/ht-movie/settings-options.php:622
msgid "Episode Runtime"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:64
msgid "Video URLs"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:70, extensions/ht-movie/settings-options.php:479
msgid "Season"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:84
msgid "All Seasons"
msgstr ""

#: extensions/ht-movie/options/posts/ht_show.php:94
msgid "Add New Season"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:6
msgid "Avatar"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:10
msgid "Avatar URL"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:14
msgid "Date of Birth"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:18
msgid "Gender"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:22
msgid "Place of Birth"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:26
msgid "Biography"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:30
msgid "Know for"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:34
msgid "Facebook Link"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:38
msgid "Twitter Link"
msgstr ""

#: extensions/ht-movie/options/taxonomies/mv_actor.php:42
msgid "Instagram Link"
msgstr ""

#: extensions/ht-movie/settings-options.php:10
msgid "Yotube Google API Key"
msgstr ""

#: extensions/ht-movie/settings-options.php:11
msgid "Enter your Google API youtube Key. Or get it <a href=\"https://developers.google.com/youtube/v3/getting-started\">here</a>"
msgstr ""

#: extensions/ht-movie/settings-options.php:15
msgid "TMDB API Key"
msgstr ""

#: extensions/ht-movie/settings-options.php:16
msgid "Enter your The Movie Database (TMDb) API Key. Or get it <a href=\"https://developers.themoviedb.org/3/getting-started/introduction\">here</a>"
msgstr ""

#: extensions/ht-movie/settings-options.php:20
msgid "Template Style"
msgstr ""

#: extensions/ht-movie/settings-options.php:21
msgid "Select style for Movie, TV Show single post."
msgstr ""

#: extensions/ht-movie/settings-options.php:25
msgid "Style 1"
msgstr ""

#: extensions/ht-movie/settings-options.php:26
msgid "Style 2"
msgstr ""

#: extensions/ht-movie/settings-options.php:30
msgid "Import Language"
msgstr ""

#: extensions/ht-movie/settings-options.php:31
msgid "Choose language support for importing."
msgstr ""

#: extensions/ht-movie/settings-options.php:35
msgid "English"
msgstr ""

#: extensions/ht-movie/settings-options.php:36
msgid "Abkhazian"
msgstr ""

#: extensions/ht-movie/settings-options.php:37
msgid "Afar"
msgstr ""

#: extensions/ht-movie/settings-options.php:38
msgid "Afrikaans"
msgstr ""

#: extensions/ht-movie/settings-options.php:39
msgid "Akan"
msgstr ""

#: extensions/ht-movie/settings-options.php:40
msgid "Albanian"
msgstr ""

#: extensions/ht-movie/settings-options.php:41
msgid "Amharic"
msgstr ""

#: extensions/ht-movie/settings-options.php:42
msgid "Arabic"
msgstr ""

#: extensions/ht-movie/settings-options.php:43
msgid "Aragonese"
msgstr ""

#: extensions/ht-movie/settings-options.php:44
msgid "Armenian"
msgstr ""

#: extensions/ht-movie/settings-options.php:45
msgid "Assamese"
msgstr ""

#: extensions/ht-movie/settings-options.php:46
msgid "Avaric"
msgstr ""

#: extensions/ht-movie/settings-options.php:47
msgid "Avestan"
msgstr ""

#: extensions/ht-movie/settings-options.php:48
msgid "Aymara"
msgstr ""

#: extensions/ht-movie/settings-options.php:49
msgid "Azerbaijani"
msgstr ""

#: extensions/ht-movie/settings-options.php:50
msgid "Bambara"
msgstr ""

#: extensions/ht-movie/settings-options.php:51
msgid "Bashkir"
msgstr ""

#: extensions/ht-movie/settings-options.php:52
msgid "Basque"
msgstr ""

#: extensions/ht-movie/settings-options.php:53
msgid "Belarusian"
msgstr ""

#: extensions/ht-movie/settings-options.php:54
msgid "Bengali (Bangla)"
msgstr ""

#: extensions/ht-movie/settings-options.php:55
msgid "Bihari"
msgstr ""

#: extensions/ht-movie/settings-options.php:56
msgid "Bislama"
msgstr ""

#: extensions/ht-movie/settings-options.php:57
msgid "Bosnian"
msgstr ""

#: extensions/ht-movie/settings-options.php:58
msgid "Breton"
msgstr ""

#: extensions/ht-movie/settings-options.php:59
msgid "Bulgarian"
msgstr ""

#: extensions/ht-movie/settings-options.php:60
msgid "Burmese"
msgstr ""

#: extensions/ht-movie/settings-options.php:61
msgid "Catalan"
msgstr ""

#: extensions/ht-movie/settings-options.php:62
msgid "Chamorro"
msgstr ""

#: extensions/ht-movie/settings-options.php:63
msgid "Chechen"
msgstr ""

#: extensions/ht-movie/settings-options.php:64
msgid "Chichewa, Chewa, Nyanja"
msgstr ""

#: extensions/ht-movie/settings-options.php:65
msgid "Chinese"
msgstr ""

#: extensions/ht-movie/settings-options.php:66
msgid "Chuvash"
msgstr ""

#: extensions/ht-movie/settings-options.php:67
msgid "Cornish"
msgstr ""

#: extensions/ht-movie/settings-options.php:68
msgid "Corsican"
msgstr ""

#: extensions/ht-movie/settings-options.php:69
msgid "Cree"
msgstr ""

#: extensions/ht-movie/settings-options.php:70
msgid "Croatian"
msgstr ""

#: extensions/ht-movie/settings-options.php:71
msgid "Czech"
msgstr ""

#: extensions/ht-movie/settings-options.php:72
msgid "Danish"
msgstr ""

#: extensions/ht-movie/settings-options.php:73
msgid "Divehi, Dhivehi, Maldivian"
msgstr ""

#: extensions/ht-movie/settings-options.php:74
msgid "Dzongkha"
msgstr ""

#: extensions/ht-movie/settings-options.php:75
msgid "Esperanto"
msgstr ""

#: extensions/ht-movie/settings-options.php:76
msgid "Estonian"
msgstr ""

#: extensions/ht-movie/settings-options.php:77
msgid "Ewe"
msgstr ""

#: extensions/ht-movie/settings-options.php:78
msgid "Faroese"
msgstr ""

#: extensions/ht-movie/settings-options.php:79
msgid "Fijian"
msgstr ""

#: extensions/ht-movie/settings-options.php:80
msgid "Finnish"
msgstr ""

#: extensions/ht-movie/settings-options.php:81
msgid "French"
msgstr ""

#: extensions/ht-movie/settings-options.php:82
msgid "Fula, Fulah, Pulaar, Pular"
msgstr ""

#: extensions/ht-movie/settings-options.php:83
msgid "Galician"
msgstr ""

#: extensions/ht-movie/settings-options.php:84
msgid "Gaelic (Scottish)"
msgstr ""

#: extensions/ht-movie/settings-options.php:85
msgid "Gaelic (Manx)"
msgstr ""

#: extensions/ht-movie/settings-options.php:86
msgid "Georgian"
msgstr ""

#: extensions/ht-movie/settings-options.php:87
msgid "German"
msgstr ""

#: extensions/ht-movie/settings-options.php:88
msgid "Greek"
msgstr ""

#: extensions/ht-movie/settings-options.php:89
msgid "Greenlandic"
msgstr ""

#: extensions/ht-movie/settings-options.php:90
msgid "Guarani"
msgstr ""

#: extensions/ht-movie/settings-options.php:91
msgid "Gujarati"
msgstr ""

#: extensions/ht-movie/settings-options.php:92
msgid "Haitian Creole"
msgstr ""

#: extensions/ht-movie/settings-options.php:93
msgid "Hausa"
msgstr ""

#: extensions/ht-movie/settings-options.php:94
msgid "Hebrew"
msgstr ""

#: extensions/ht-movie/settings-options.php:95
msgid "Herero"
msgstr ""

#: extensions/ht-movie/settings-options.php:96
msgid "Hindi"
msgstr ""

#: extensions/ht-movie/settings-options.php:97
msgid "Hiri Motu"
msgstr ""

#: extensions/ht-movie/settings-options.php:98
msgid "Hungarian"
msgstr ""

#: extensions/ht-movie/settings-options.php:99
msgid "Icelandic"
msgstr ""

#: extensions/ht-movie/settings-options.php:100
msgid "Ido"
msgstr ""

#: extensions/ht-movie/settings-options.php:101
msgid "Igbo"
msgstr ""

#: extensions/ht-movie/settings-options.php:102
msgid "Indonesian"
msgstr ""

#: extensions/ht-movie/settings-options.php:103
msgid "Interlingua"
msgstr ""

#: extensions/ht-movie/settings-options.php:104
msgid "Interlingue"
msgstr ""

#: extensions/ht-movie/settings-options.php:105
msgid "Inuktitut"
msgstr ""

#: extensions/ht-movie/settings-options.php:106
msgid "Inupiak"
msgstr ""

#: extensions/ht-movie/settings-options.php:107
msgid "Irish"
msgstr ""

#: extensions/ht-movie/settings-options.php:108
msgid "Italian"
msgstr ""

#: extensions/ht-movie/settings-options.php:109
msgid "Japanese"
msgstr ""

#: extensions/ht-movie/settings-options.php:110
msgid "Javanese"
msgstr ""

#: extensions/ht-movie/settings-options.php:111
msgid "Kalaallisut, Greenlandic"
msgstr ""

#: extensions/ht-movie/settings-options.php:112
msgid "Kannada"
msgstr ""

#: extensions/ht-movie/settings-options.php:113
msgid "Kanuri"
msgstr ""

#: extensions/ht-movie/settings-options.php:114
msgid "Kashmiri"
msgstr ""

#: extensions/ht-movie/settings-options.php:115
msgid "Kazakh"
msgstr ""

#: extensions/ht-movie/settings-options.php:116
msgid "Khmer"
msgstr ""

#: extensions/ht-movie/settings-options.php:117
msgid "Kikuyu"
msgstr ""

#: extensions/ht-movie/settings-options.php:118
msgid "Kinyarwanda (Rwanda)"
msgstr ""

#: extensions/ht-movie/settings-options.php:119
msgid "Kirundi"
msgstr ""

#: extensions/ht-movie/settings-options.php:120
msgid "Kyrgyz"
msgstr ""

#: extensions/ht-movie/settings-options.php:121
msgid "Komi"
msgstr ""

#: extensions/ht-movie/settings-options.php:122
msgid "Kongo"
msgstr ""

#: extensions/ht-movie/settings-options.php:123
msgid "Korean"
msgstr ""

#: extensions/ht-movie/settings-options.php:124
msgid "Kurdish"
msgstr ""

#: extensions/ht-movie/settings-options.php:125
msgid "Kwanyama"
msgstr ""

#: extensions/ht-movie/settings-options.php:126
msgid "Lao"
msgstr ""

#: extensions/ht-movie/settings-options.php:127
msgid "Latin"
msgstr ""

#: extensions/ht-movie/settings-options.php:128
msgid "Latvian (Lettish)"
msgstr ""

#: extensions/ht-movie/settings-options.php:129
msgid "Limburgish ( Limburger)"
msgstr ""

#: extensions/ht-movie/settings-options.php:130
msgid "Lingala"
msgstr ""

#: extensions/ht-movie/settings-options.php:131
msgid "Lithuanian"
msgstr ""

#: extensions/ht-movie/settings-options.php:132
msgid "Luganda, Ganda"
msgstr ""

#: extensions/ht-movie/settings-options.php:133
msgid "Luxembourgish"
msgstr ""

#: extensions/ht-movie/settings-options.php:134
msgid "Manx"
msgstr ""

#: extensions/ht-movie/settings-options.php:135
msgid "Macedonian"
msgstr ""

#: extensions/ht-movie/settings-options.php:136
msgid "Malagasy"
msgstr ""

#: extensions/ht-movie/settings-options.php:137
msgid "Malay"
msgstr ""

#: extensions/ht-movie/settings-options.php:138
msgid "Malayalam"
msgstr ""

#: extensions/ht-movie/settings-options.php:139
msgid "Maltese"
msgstr ""

#: extensions/ht-movie/settings-options.php:140
msgid "Maori"
msgstr ""

#: extensions/ht-movie/settings-options.php:141
msgid "Marathi"
msgstr ""

#: extensions/ht-movie/settings-options.php:142
msgid "Marshallese"
msgstr ""

#: extensions/ht-movie/settings-options.php:143
msgid "Moldavian"
msgstr ""

#: extensions/ht-movie/settings-options.php:144
msgid "Mongolian"
msgstr ""

#: extensions/ht-movie/settings-options.php:145
msgid "Nauru"
msgstr ""

#: extensions/ht-movie/settings-options.php:146
msgid "Navajo"
msgstr ""

#: extensions/ht-movie/settings-options.php:147
msgid "Ndonga"
msgstr ""

#: extensions/ht-movie/settings-options.php:148
msgid "Northern Ndebele"
msgstr ""

#: extensions/ht-movie/settings-options.php:149
msgid "Nepali"
msgstr ""

#: extensions/ht-movie/settings-options.php:150
msgid "Norwegian"
msgstr ""

#: extensions/ht-movie/settings-options.php:151
msgid "Norwegian bokmal"
msgstr ""

#: extensions/ht-movie/settings-options.php:152
msgid "Norwegian nynorsk	"
msgstr ""

#: extensions/ht-movie/settings-options.php:153
msgid "Nuosu"
msgstr ""

#: extensions/ht-movie/settings-options.php:154
msgid "Occitan"
msgstr ""

#: extensions/ht-movie/settings-options.php:155
msgid "Ojibwe"
msgstr ""

#: extensions/ht-movie/settings-options.php:156
msgid "Old Church Slavonic, Old Bulgarian"
msgstr ""

#: extensions/ht-movie/settings-options.php:157
msgid "Oriya"
msgstr ""

#: extensions/ht-movie/settings-options.php:158
msgid "Oromo (Afaan Oromo)"
msgstr ""

#: extensions/ht-movie/settings-options.php:159
msgid "Ossetian"
msgstr ""

#: extensions/ht-movie/settings-options.php:160
msgid "Pali"
msgstr ""

#: extensions/ht-movie/settings-options.php:161
msgid "Pashto, Pushto"
msgstr ""

#: extensions/ht-movie/settings-options.php:162
msgid "Persian (Farsi)"
msgstr ""

#: extensions/ht-movie/settings-options.php:163
msgid "Polish"
msgstr ""

#: extensions/ht-movie/settings-options.php:164
msgid "Portuguese"
msgstr ""

#: extensions/ht-movie/settings-options.php:165
msgid "Punjabi (Eastern)"
msgstr ""

#: extensions/ht-movie/settings-options.php:166
msgid "Quechua"
msgstr ""

#: extensions/ht-movie/settings-options.php:167
msgid "Romansh"
msgstr ""

#: extensions/ht-movie/settings-options.php:168
msgid "Romanian"
msgstr ""

#: extensions/ht-movie/settings-options.php:169
msgid "Russian"
msgstr ""

#: extensions/ht-movie/settings-options.php:170
msgid "Sami"
msgstr ""

#: extensions/ht-movie/settings-options.php:171
msgid "Samoan"
msgstr ""

#: extensions/ht-movie/settings-options.php:172
msgid "Sango"
msgstr ""

#: extensions/ht-movie/settings-options.php:173
msgid "Sanskrit"
msgstr ""

#: extensions/ht-movie/settings-options.php:174
msgid "Serbian"
msgstr ""

#: extensions/ht-movie/settings-options.php:175
msgid "Serbo-Croatian"
msgstr ""

#: extensions/ht-movie/settings-options.php:176
msgid "Sesotho"
msgstr ""

#: extensions/ht-movie/settings-options.php:177
msgid "Setswana"
msgstr ""

#: extensions/ht-movie/settings-options.php:178
msgid "Sichuan Yi"
msgstr ""

#: extensions/ht-movie/settings-options.php:179
msgid "Sindhi"
msgstr ""

#: extensions/ht-movie/settings-options.php:180
msgid "Sinhalese"
msgstr ""

#: extensions/ht-movie/settings-options.php:181
msgid "Siswati"
msgstr ""

#: extensions/ht-movie/settings-options.php:182
msgid "Slovak"
msgstr ""

#: extensions/ht-movie/settings-options.php:183
msgid "Slovenian"
msgstr ""

#: extensions/ht-movie/settings-options.php:184
msgid "Somali"
msgstr ""

#: extensions/ht-movie/settings-options.php:185
msgid "Southern Ndebele"
msgstr ""

#: extensions/ht-movie/settings-options.php:186
msgid "Spanish"
msgstr ""

#: extensions/ht-movie/settings-options.php:187
msgid "Sundanese"
msgstr ""

#: extensions/ht-movie/settings-options.php:188
msgid "Swahili (Kiswahili)"
msgstr ""

#: extensions/ht-movie/settings-options.php:189
msgid "Swati"
msgstr ""

#: extensions/ht-movie/settings-options.php:190
msgid "Swedish"
msgstr ""

#: extensions/ht-movie/settings-options.php:191
msgid "Tagalog"
msgstr ""

#: extensions/ht-movie/settings-options.php:192
msgid "Tahitian"
msgstr ""

#: extensions/ht-movie/settings-options.php:193
msgid "Tajik"
msgstr ""

#: extensions/ht-movie/settings-options.php:194
msgid "Tamil"
msgstr ""

#: extensions/ht-movie/settings-options.php:195
msgid "Tatar"
msgstr ""

#: extensions/ht-movie/settings-options.php:196
msgid "Telugu"
msgstr ""

#: extensions/ht-movie/settings-options.php:197
msgid "Thai"
msgstr ""

#: extensions/ht-movie/settings-options.php:198
msgid "Tibetan"
msgstr ""

#: extensions/ht-movie/settings-options.php:199
msgid "Tigrinya"
msgstr ""

#: extensions/ht-movie/settings-options.php:200
msgid "Tonga"
msgstr ""

#: extensions/ht-movie/settings-options.php:201
msgid "Tsonga"
msgstr ""

#: extensions/ht-movie/settings-options.php:202
msgid "Turkish"
msgstr ""

#: extensions/ht-movie/settings-options.php:203
msgid "Turkmen"
msgstr ""

#: extensions/ht-movie/settings-options.php:204
msgid "Twi"
msgstr ""

#: extensions/ht-movie/settings-options.php:205
msgid "Uyghur"
msgstr ""

#: extensions/ht-movie/settings-options.php:206
msgid "Ukrainian"
msgstr ""

#: extensions/ht-movie/settings-options.php:207
msgid "Urdu"
msgstr ""

#: extensions/ht-movie/settings-options.php:208
msgid "Uzbek"
msgstr ""

#: extensions/ht-movie/settings-options.php:209
msgid "Venda"
msgstr ""

#: extensions/ht-movie/settings-options.php:210
msgid "Vietnamese"
msgstr ""

#: extensions/ht-movie/settings-options.php:211
msgid "Volapuk"
msgstr ""

#: extensions/ht-movie/settings-options.php:212
msgid "Wallon"
msgstr ""

#: extensions/ht-movie/settings-options.php:213
msgid "Welsh"
msgstr ""

#: extensions/ht-movie/settings-options.php:214
msgid "Wolof"
msgstr ""

#: extensions/ht-movie/settings-options.php:215
msgid "Western Frisian"
msgstr ""

#: extensions/ht-movie/settings-options.php:216
msgid "Xhosa"
msgstr ""

#: extensions/ht-movie/settings-options.php:217
msgid "Yiddish"
msgstr ""

#: extensions/ht-movie/settings-options.php:218
msgid "Yoruba"
msgstr ""

#: extensions/ht-movie/settings-options.php:219
msgid "Zhuang, Chuang"
msgstr ""

#: extensions/ht-movie/settings-options.php:220
msgid "Zulu"
msgstr ""

#: extensions/ht-movie/settings-options.php:225
msgid "Search Options"
msgstr ""

#: extensions/ht-movie/settings-options.php:226
msgid "Select at least one search option."
msgstr ""

#: extensions/ht-movie/settings-options.php:237
msgid "News"
msgstr ""

#: extensions/ht-movie/settings-options.php:245
msgid "VC in Movie single"
msgstr ""

#: extensions/ht-movie/settings-options.php:246
msgid "This option will support Visual Page builder in Movie single. You need to enable VC for Movie single at first before using this option"
msgstr ""

#: extensions/ht-movie/settings-options.php:249, extensions/ht-movie/settings-options.php:272, extensions/ht-movie/settings-options.php:285, extensions/ht-movie/settings-options.php:298, extensions/ht-movie/settings-options.php:311, extensions/ht-movie/settings-options.php:324, extensions/ht-movie/settings-options.php:337, extensions/ht-movie/settings-options.php:350, extensions/ht-movie/settings-options.php:363, extensions/ht-movie/settings-options.php:376, extensions/ht-movie/settings-options.php:389, extensions/ht-movie/settings-options.php:402, extensions/ht-movie/settings-options.php:415, extensions/ht-movie/settings-options.php:428, extensions/ht-movie/settings-options.php:441, extensions/ht-movie/settings-options.php:454, extensions/ht-movie/settings-options.php:467, extensions/ht-movie/settings-options.php:486, extensions/ht-movie/settings-options.php:499, extensions/ht-movie/settings-options.php:512, extensions/ht-movie/settings-options.php:525, extensions/ht-movie/settings-options.php:538, extensions/ht-movie/settings-options.php:551, extensions/ht-movie/settings-options.php:564, extensions/ht-movie/settings-options.php:577, extensions/ht-movie/settings-options.php:590, extensions/ht-movie/settings-options.php:603, extensions/ht-movie/settings-options.php:616, extensions/ht-movie/settings-options.php:629, extensions/ht-movie/settings-options.php:642, extensions/ht-movie/settings-options.php:655, extensions/ht-movie/settings-options.php:668
msgid "Enable"
msgstr ""

#: extensions/ht-movie/settings-options.php:253, extensions/ht-movie/settings-options.php:268, extensions/ht-movie/settings-options.php:281, extensions/ht-movie/settings-options.php:294, extensions/ht-movie/settings-options.php:307, extensions/ht-movie/settings-options.php:320, extensions/ht-movie/settings-options.php:333, extensions/ht-movie/settings-options.php:346, extensions/ht-movie/settings-options.php:359, extensions/ht-movie/settings-options.php:372, extensions/ht-movie/settings-options.php:385, extensions/ht-movie/settings-options.php:398, extensions/ht-movie/settings-options.php:411, extensions/ht-movie/settings-options.php:424, extensions/ht-movie/settings-options.php:437, extensions/ht-movie/settings-options.php:450, extensions/ht-movie/settings-options.php:463, extensions/ht-movie/settings-options.php:482, extensions/ht-movie/settings-options.php:495, extensions/ht-movie/settings-options.php:508, extensions/ht-movie/settings-options.php:521, extensions/ht-movie/settings-options.php:534, extensions/ht-movie/settings-options.php:547, extensions/ht-movie/settings-options.php:560, extensions/ht-movie/settings-options.php:573, extensions/ht-movie/settings-options.php:586, extensions/ht-movie/settings-options.php:599, extensions/ht-movie/settings-options.php:612, extensions/ht-movie/settings-options.php:625, extensions/ht-movie/settings-options.php:638, extensions/ht-movie/settings-options.php:651, extensions/ht-movie/settings-options.php:664
msgid "Disable"
msgstr ""

#: extensions/ht-movie/settings-options.php:278
msgid "Cast more infomation"
msgstr ""

#: extensions/ht-movie/settings-options.php:317, extensions/ht-movie/settings-options.php:531
msgid "Poster"
msgstr ""

#: extensions/ht-movie/settings-options.php:343, extensions/ht-movie/settings-options.php:557
msgid "Trailer"
msgstr ""

#: extensions/ht-movie/settings-options.php:460, extensions/ht-movie/settings-options.php:661
msgid "Language"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:66, extensions/ht-movie/views/single-movie.php:375, extensions/ht-movie/views/single-movie.php:626, extensions/ht-movie/views/single-movie.php:663, extensions/ht-movie/views/single-show.php:355, extensions/ht-movie/views/single-show.php:650, extensions/ht-movie/views/single-show.php:687
msgid "Actor Avatar"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:105
msgid " biography"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:106
msgid "filmography"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:113
msgid "Biography of"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:121
msgid "See all bio"
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:133
msgid "Date of Birth: "
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:139
msgid "Gender: "
msgstr ""

#: extensions/ht-movie/views/mv_actor.php:145
msgid "Place of Birth: "
msgstr ""

#. translators: %d: total results
#. translators: %d: total results
#. translators: %d: total results
#. translators: %d: total results
#: extensions/ht-movie/views/mv_collection.php:39, extensions/ht-movie/views/mv_collection.php:211, extensions/ht-movie/views/mv_genre.php:52, extensions/ht-movie/views/mv_genre.php:205
msgid "Showing all %s movie"
msgid_plural "Showing all %s movies"
msgstr[0] ""
msgstr[1] ""

#. translators: 1: first result 2: last result 3: total results
#. translators: 1: first result 2: last result 3: total results
#. translators: 1: first result 2: last result 3: total results
#. translators: 1: first result 2: last result 3: total results
#: extensions/ht-movie/views/mv_collection.php:42, extensions/ht-movie/views/mv_collection.php:214, extensions/ht-movie/views/mv_genre.php:55, extensions/ht-movie/views/mv_genre.php:208
msgctxt "blockter"
msgid "Found <strong>%s movie</strong> in total"
msgid_plural "Found <strong>%s movies</strong> in total"
msgstr[0] ""
msgstr[1] ""

#: extensions/ht-movie/views/mv_collection.php:50, extensions/ht-movie/views/mv_collection.php:222, extensions/ht-movie/views/mv_genre.php:62, extensions/ht-movie/views/mv_genre.php:215
msgid "Sort By:"
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:129, extensions/ht-movie/views/mv_collection.php:299, extensions/ht-movie/views/mv_genre.php:118, extensions/ht-movie/views/mv_genre.php:277
msgid "Read more"
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:143, extensions/ht-movie/views/mv_collection.php:313, extensions/ht-movie/views/mv_genre.php:133, extensions/ht-movie/views/mv_genre.php:292, extensions/ht-movie/views/single-movie.php:169, extensions/ht-movie/views/single-movie.php:754, extensions/ht-movie/views/single-show.php:156, extensions/ht-movie/views/single-show.php:778
msgid "/"
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:161, extensions/ht-movie/views/mv_collection.php:331, extensions/ht-movie/views/mv_genre.php:152, extensions/ht-movie/views/mv_genre.php:311, extensions/ht-movie/views/single-movie.php:772, extensions/ht-movie/views/single-show.php:796
msgid "Run time: "
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:164, extensions/ht-movie/views/mv_collection.php:334, extensions/ht-movie/views/mv_genre.php:155, extensions/ht-movie/views/mv_genre.php:314, extensions/ht-movie/views/single-movie.php:775, extensions/ht-movie/views/single-show.php:799
msgid "Tagline: "
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:167, extensions/ht-movie/views/mv_collection.php:337, extensions/ht-movie/views/mv_genre.php:158, extensions/ht-movie/views/mv_genre.php:317, extensions/ht-movie/views/single-movie.php:778, extensions/ht-movie/views/single-show.php:802
msgid "Release: "
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:171, extensions/ht-movie/views/mv_collection.php:341, extensions/ht-movie/views/mv_genre.php:162, extensions/ht-movie/views/mv_genre.php:321, extensions/ht-movie/views/single-movie.php:242, extensions/ht-movie/views/single-movie.php:474, extensions/ht-movie/views/single-movie.php:783, extensions/ht-movie/views/single-show.php:807
msgid "Director: "
msgstr ""

#: extensions/ht-movie/views/mv_collection.php:175, extensions/ht-movie/views/mv_collection.php:345, extensions/ht-movie/views/mv_genre.php:166, extensions/ht-movie/views/mv_genre.php:325, extensions/ht-movie/views/single-movie.php:257, extensions/ht-movie/views/single-movie.php:789, extensions/ht-movie/views/single-show.php:240, extensions/ht-movie/views/single-show.php:813
msgid "Stars: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:127, extensions/ht-movie/views/single-show.php:122
msgid "Favourited"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:130, extensions/ht-movie/views/single-show.php:124
msgid "Favourite"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:146, extensions/ht-movie/views/single-show.php:133
msgid "share"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:177, extensions/ht-movie/views/single-show.php:164
msgid "1 Review"
msgid_plural "%1$s Reviews"
msgstr[0] ""
msgstr[1] ""

#: extensions/ht-movie/views/single-movie.php:184, extensions/ht-movie/views/single-show.php:171
msgid "Rate this movie: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:198
msgid "Related Movies"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:199, extensions/ht-movie/views/single-show.php:188
msgid "Reviews"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:222, extensions/ht-movie/views/single-movie.php:488
msgid "Release Date: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:228, extensions/ht-movie/views/single-movie.php:495
msgid "Run Time: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:234, extensions/ht-movie/views/single-movie.php:502, extensions/ht-movie/views/single-show.php:217
msgid "Tagline:"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:248, extensions/ht-movie/views/single-movie.php:481, extensions/ht-movie/views/single-show.php:461
msgid "Writer: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:266, extensions/ht-movie/views/single-movie.php:509, extensions/ht-movie/views/single-show.php:249, extensions/ht-movie/views/single-show.php:489
msgid "Genres: "
msgstr ""

#: extensions/ht-movie/views/single-movie.php:303, extensions/ht-movie/views/single-show.php:280
msgid "View all media"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:335, extensions/ht-movie/views/single-movie.php:561, extensions/ht-movie/views/single-show.php:309, extensions/ht-movie/views/single-show.php:577
msgid "No Title"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:352, extensions/ht-movie/views/single-show.php:332
msgid "View all cast"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:401, extensions/ht-movie/views/single-show.php:381
msgid "Review"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:402, extensions/ht-movie/views/single-show.php:382
msgid "View all reviews"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:440, extensions/ht-movie/views/single-show.php:420
msgctxt "1: date, 2: time"
msgid "- %1$s at %2$s"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:528, extensions/ht-movie/views/single-show.php:551
msgid "Videos"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:575, extensions/ht-movie/views/single-show.php:599
msgid "Photos"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:714
msgid "Related Movies To"
msgstr ""

#: extensions/ht-movie/views/single-movie.php:816, extensions/ht-movie/views/single-show.php:840
msgid "Reviews for"
msgstr ""

#: extensions/ht-movie/views/single-show.php:184
msgid "Seasons"
msgstr ""

#: extensions/ht-movie/views/single-show.php:187
msgid "Related Shows"
msgstr ""

#: extensions/ht-movie/views/single-show.php:205
msgid "First Air Date: "
msgstr ""

#: extensions/ht-movie/views/single-show.php:211, extensions/ht-movie/views/single-show.php:475
msgid "Episode Runtime: "
msgstr ""

#: extensions/ht-movie/views/single-show.php:225, extensions/ht-movie/views/single-show.php:454
msgid "Creator: "
msgstr ""

#: extensions/ht-movie/views/single-show.php:231
msgid "Production Companies: "
msgstr ""

#: extensions/ht-movie/views/single-show.php:468
msgid "Fist Air Date: "
msgstr ""

#: extensions/ht-movie/views/single-show.php:482
msgid "Production Companies:"
msgstr ""

#: extensions/ht-movie/views/single-show.php:521
msgid "Season %d"
msgstr ""

#: extensions/ht-movie/views/single-show.php:526
msgid "%d Episodes"
msgstr ""

#: extensions/ht-movie/views/single-show.php:530
msgid "Air Date: %s"
msgstr ""

#: extensions/ht-movie/views/single-show.php:738
msgid "Related Shows To"
msgstr ""
