<?php
/**
 * Widget Collections
 *
 * @package Blockter
 */

class Widget_Collections extends WP_Widget {
	/**
	 * Set up new Collections widget instance.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'blockter_widget_collections',
			'description' => __( 'A list of collections', 'blockter' ),
		);
		parent::__construct( 'collections', __( 'Collections', 'blockter' ), $widget_ops );
	}

	/**
	 * Output the content for the current Collections widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Collections', 'blockter' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$include = empty( $instance['include'] ) ? '' : $instance['include'];

		$show_count = ! empty( $instance['count'] ) ? true : false;

		$collections = get_terms(
			array(
				'taxonomy' => 'mv_collection',
				'include'  => $include,
				'count'    => $show_count,
			)
		);

		if ( empty( $collections ) || is_wp_error( $collections ) ) {
			return;
		}

		echo esc_html( $args['before_widget'] );

		if ( $title ) {
			echo esc_html( $args['before_title'] . $title . $args['after_title'] );
		}

		?>
		<ul>
			<?php
			foreach ( $collections as $collection ) :
				$collection_id   = $collection->term_id;
				$collection_name = $collection->name;
				$collection_link = get_term_link( $collection );

				if ( $show_count ) {
					$collection_count = $collection->count;
				}
				?>
				<li class="<?php printf( esc_attr( 'collection-item collection-item-%d' ), $collection_id ); ?>">
					<a href="<?php echo esc_url( $collection_link ); ?>"><?php echo esc_html( $collection_name ); ?></a>
					<?php if ( $show_count ) : ?>
						<span><?php printf( esc_html( '(%d)' ), $collection_count ); ?></span>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php
		echo esc_html( $args['after_widget'] );
	}


	/**
	 * Handles updating the settings for the current Collections widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user.
	 * @param  array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance            = $old_instance;
		$instance['title']   = sanitize_text_field( $new_instance['title'] );
		$instance['include'] = sanitize_text_field( $new_instance['include'] );
		$instance['count']   = ! empty( $new_instance['count'] ) ? true : false;

		return $instance;
	}

	/**
	 * Outputs the settings form for the Collections widget.
	 */

	public function form( $instance ) {
		$title      = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$include    = isset( $instance['include'] ) ? esc_attr( $instance['include'] ) : '';
		$show_count = isset( $instance['count'] ) ? (bool) $instance['count'] : false;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'blockter' ); ?></label>
			<input
				type="text"
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"><?php esc_html_e( 'Include', 'blockter' ); ?></label>
			<input
				type="text"
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'include' ) ); ?>"
				value="<?php echo esc_attr( $include ); ?>">
			<br>
			<small><?php esc_html_e( 'Collection IDs, seperated by commas.', 'blockter' ); ?></small>
		</p>
		<p>
			<input
				type="checkbox"
				class="checkbox"
				<?php checked( $show_count ); ?>
				id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php esc_html_e( 'Show collection counts', 'blockter' ); ?></label>
		</p>
		<?php
	}
}
