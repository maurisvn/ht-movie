<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * @var $instance
 * @var $before_widget
 * @var $after_widget
 * @var $title
 */
?>
<?php if ( ! empty( $instance ) ) : ?>
	<?php echo esc_html( $before_widget ); ?>
	<div class="wrap-social">
		<?php echo esc_html( $title ); ?>
		<div class="theme-twitter-carousel twitter-carousel">
			<?php foreach ( $instance as $key => $value ) :
				if ( empty( $value ) ) {
					continue;
				}
				?>
				<div class="twitter-it">
					<div class="twitter-it tweet" id="<?php echo esc_html( $value ); ?>"></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php echo esc_html( $after_widget );?>
<?php endif; ?>
