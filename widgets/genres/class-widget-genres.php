<?php
/**
 * Widget Genres
 *
 * @package Blockter
 */

class Widget_Genres extends WP_Widget {
	/**
	 * Set up new Genres widget instance.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'blockter_widget_genres',
			'description' => __( 'A list of genres', 'blockter' ),
		);
		parent::__construct( 'genres', __( 'Genres', 'blockter' ), $widget_ops );
	}

	/**
	 * Output the content for the current Genres widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Genres', 'blockter' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$include = empty( $instance['include'] ) ? '' : $instance['include'];

		$show_count = ! empty( $instance['count'] ) ? true : false;

		$genres = get_terms(
			array(
				'taxonomy' => 'mv_genre',
				'include'  => $include,
				'count'    => $show_count,
			)
		);

		if ( empty( $genres ) || is_wp_error( $genres ) ) {
			return;
		}

		echo esc_html( $args['before_widget'] );

		if ( $title ) {
			echo esc_html( $args['before_title'] . $title . $args['after_title'] );
		}

		?>
		<ul>
			<?php
			foreach ( $genres as $genre ) :
				$genre_id   = $genre->term_id;
				$genre_name = $genre->name;
				$genre_link = get_term_link( $genre );

				if ( $show_count ) {
					$genre_count = $genre->count;
				}
				?>
			<li class="<?php printf( esc_attr( 'genre-item genre-item-%d' ), $genre_id ); ?>">
				<a href="<?php echo esc_url( $genre_link ); ?>"><?php echo esc_html( $genre_name ); ?></a>
				<?php if ( $show_count ) : ?>
				<span><?php printf( esc_html( '(%d)' ), $genre_count ); ?></span>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php
		echo esc_html( $args['after_widget'] );
	}


	/**
	 * Handles updating the settings for the current Genres widget instance.
	 *
	 * @param array $new_instance New settings for this instance as input by the user.
	 * @param  array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance            = $old_instance;
		$instance['title']   = sanitize_text_field( $new_instance['title'] );
		$instance['include'] = sanitize_text_field( $new_instance['include'] );
		$instance['count']   = ! empty( $new_instance['count'] ) ? true : false;

		return $instance;
	}

	/**
	 * Outputs the settings form for the Genres widget.
	 */

	public function form( $instance ) {
		$title      = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$include    = isset( $instance['include'] ) ? esc_attr( $instance['include'] ) : '';
		$show_count = isset( $instance['count'] ) ? (bool) $instance['count'] : false;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'blockter' ); ?></label>
			<input
				type="text"
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
				value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"><?php esc_html_e( 'Include', 'blockter' ); ?></label>
			<input
				type="text"
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'include' ) ); ?>"
				value="<?php echo esc_attr( $include ); ?>">
			<br>
			<small><?php esc_html_e( 'Genre IDs, seperated by commas.', 'blockter' ); ?></small>
		</p>
		<p>
			<input
				type="checkbox"
				class="checkbox"
				<?php checked( $show_count ); ?>
				id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php esc_html_e( 'Show genre counts', 'blockter' ); ?></label>
		</p>
		<?php
	}
}
