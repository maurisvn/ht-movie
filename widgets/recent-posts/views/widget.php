<?php
/**
 * @var string $before_widget
 * @var string $after_widget
 * @var string $title
 * @var string $number
 */

echo wp_kses_post($before_widget );
echo wp_kses_post($title);
$query = new WP_Query('post_type=post&post_status=publish&ignore_sticky_posts=true&posts_per_page='.$number);
$num = 1;

if( $query->have_posts() ):
    echo '<div class="flw">';
    while($query->have_posts()): $query->the_post();

?>
    <div class="widget_recent_post_thumbnail_item flw">
	
	    <div class="blog-recent-post-thumbnail-sumary">
	    	<?php $num = str_pad($num, 2, '0', STR_PAD_LEFT); ?>
	    	<span><?php echo esc_html($num);?></span>
            <div class="post-tt"><a class="post-tit" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
		</div>
    </div>
<?php
	$num++;
    endwhile;
    echo '</div>';
endif;
wp_reset_postdata();
echo wp_kses_post($after_widget);