<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * @var $instance
 * @var $before_widget
 * @var $after_widget
 * @var $title
 */
?>
<?php if ( ! empty( $instance ) ) : ?>
	<?php echo esc_html( $before_widget );?>
	<div class="wrap-social">
		<?php echo esc_html( $title ); ?>
		<ul>
			<?php foreach ( $instance as $key => $value ) :
				if ( empty( $value ) ) {
					continue;
				}
				?>
				<li><iframe src="<?php echo esc_url( $value );?>" data-src="<?php echo esc_url( $value ); ?>" width="300" height="315" style="border:none;overflow:hidden" ></iframe></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php echo esc_html( $after_widget );?>
<?php endif; ?>
