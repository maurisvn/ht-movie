<?php
/**
 * @var string $before_widget
 * @var string $after_widget
 * @var string $title
 * @var string $number
 */
echo wp_kses_post($before_widget );
echo wp_kses_post($title);
?>
 <?php $terms_casts = get_terms('mv_actor');?>
	<?php $terms = array_slice($terms_casts, 0, $number, true);?>
	<div class="flw">
		<?php foreach($terms as $term): ?>
			<?php
				$term_id = $term->term_id;
				$cast_name = $term->name;
				$cast_url = get_term_link($term);
				$cast_avatar = fw_get_db_term_option($term_id, 'mv_actor');  
				$cast_terms = fw_get_db_term_option($term_id, 'mv_actor');
			?>
			<div class="widget_recent_celebrity_item flw">
					<div class="celebrity-thumbnail">
						<?php
							if ( array_key_exists('avatar_url', $cast_avatar ) && $cast_avatar['avatar_url'] != '' ) :
						?>
							<a class="celebrity-img" href="<?php echo esc_url($cast_url);?>">
								<img
									src="<?php echo esc_attr( $cast_avatar['avatar_url'] ); ?>"
									alt="<?php echo esc_attr__( 'Actor Avatar', 'blockter' ); ?>"
								>
							</a>
						<?php elseif ( array_key_exists('avatar', $cast_avatar ) && $cast_avatar['avatar'] != '' ) : ?>
							<?php $att_id = $cast_avatar['avatar']['attachment_id'];?>
							<a class="celebrity-img" href="<?php echo esc_url($cast_url);?>">
								<?php echo wp_get_attachment_image( $att_id, array(70, 70) );?>
							</a>
						<?php else: ?>
							<a  class="celebrity-img" href="<?php echo esc_url($cast_url);?>">
								<div class="no-image"></div>
							</a>
						<?php endif; ?>
				</div>
				<div class="celebrity-summary">
					<div><a class="actor-name" href="<?php echo esc_url($cast_url);?>"><?php echo esc_html($cast_name); ?></a></div>
					<?php if (array_key_exists( 'knowfor', $cast_terms ) && isset( $cast_terms['knowfor'] ) ) : ?>
						<dt class="celebrity-pos"><?php echo esc_html( $cast_terms['knowfor'] ); ?></dt>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php
wp_reset_postdata();
echo wp_kses_post($after_widget);