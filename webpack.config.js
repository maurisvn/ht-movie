const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const config = {
	watch: true,
	entry: {
		app: './src/js/app.js',
		vendor: ['vue'],
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'extensions/ht-movie/static/js/'),
		publicPath: './extensions/ht-movie/static',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader',
			},
			{
				test: /\.css$/,
				loader: ['style-loader', 'css-loader'],
			},
		],
	},
	resolve: {
		extensions: ['.js'],
		alias: {
			vue$: 'vue/dist/vue.esm.js',
		},
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			names: ['vendor'],
		}),
	],
};

//If true JS files will be minified
if (process.env.NODE_ENV === 'production') {
	config.plugins.push(
		new UglifyJSPlugin({
			compress: {
				warnings: false,
			},
		})
	);
	config.watch = false;
}

module.exports = config;
