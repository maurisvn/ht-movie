(function($){
	"use strict";
    $(document).ready( function() {
        var favourite = $('.favorite-btn button');
        var user_id = favourite.data('user');
        var post_id = favourite.data('post');
        var show_id = favourite.data('tvshow');
        favourite.on('click',function(event){
        	var favourite_text_change = favourite.find('.favourite-text').text();
        	$.ajax({
            	url : favourite_params.ajaxurl,
            	type:'POST',
                data : {
                    action: 'buster_add_favourite',
                    user_id: user_id,
                    post_id: post_id,
                    show_id: show_id,
                },
                error: function(xhr){
        			alert("An error occured: " + xhr.status + " " + xhr.statusText);
        		},
            	success : function( data ){
	               	favourite.toggleClass('favourited');
		            favourite.find('.favourite-text').text(
		            	 favourite_text_change == "Favourited" ? "Favourite" : "Favourited"
		            );
	            },
            });
        });
    });
})(jQuery);